#!/usr/bin/make -f

all: simons_genome_diversity_oct_2016.pdf

R ?= R

%.pdf: %.svg
	inkscape -A $@ $<
	pdfcrop $@
	mv $(dir $@)*-crop.pdf $@

%.png: %.svg
	inkscape -e $@ -d 300 $<

%.tex: %.Rnw
	$(R) --encoding=utf-8 -e "library('knitr'); knit('$<')"

simons_genome_diversity_oct_2016.pdf:									\
simons_genome_diversity_oct_2016.tex genome_diversity_paper figures

%.pdf: %.tex $(wildcard *.bib) $(wildcard *.tex)
	latexmk -f -pdf -pdflatex='xelatex -shell-escape -8bit -interaction=nonstopmode %O %S' -bibtex -use-make $<


